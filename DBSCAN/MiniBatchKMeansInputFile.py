import pandas as pd
import numpy as np
import pickle

from sklearn.cluster import KMeans
from pathlib import Path
from sklearn import metrics

#    For Printing of Data, All features are not shown Horizontally.
pd.set_option('display.height', 1000)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

UniversityList = []
Binned_Input_Array = []

########################            PreTrained Model Files              ########################################
#   Loading Computer Science Model File
CSModelFile = Path('D:\BTP\MiniBatchCSTrainedData\SavedMiniBatchCSModel.pkl')

#   Loading Electrical Engineering Model File
ElectricalModelFile = Path('D:\BTP\Electrical\SavedModel.pkl')

#   Loading Industrail Engineering Model File
IndustrialModelFile = Path('D:\BTP\Industrial_Engineering\SavedModel.pkl')

#   Loading Mechanical Engineering Model File
MechanicalModelFile = Path('D:\BTP\Mechanical_Engineering\SavedModel.pkl')
##########################################################################################

########################            PreTrained Labeled Data           ########################################
#   Loading Computer Science Model File
CSData = Path('D:\BTP\MiniBatchCSTrainedData\SavedMiniBatchCSData.pkl')

#   Loading Electrical Engineering Model File
ElectricalData = Path('D:\BTP\Electrical\SavedData.pkl')

#   Loading Industrail Engineering Model File
IndustrialData = Path('D:\BTP\Industrial_Engineering\SavedData.pkl')

#   Loading Mechanical Engineering Model File
MechanicalData = Path('D:\BTP\Mechanical_Engineering\SavedData.pkl')
##########################################################################################

def UpdateModel(model,ModelFile):
    pickle.dump(model, open(ModelFile, 'wb+'))


########################            Loading PreTrained Labeled         ########################################
#   Loading CSData
def LoadCSData():
    with open(CSData,'rb') as file:
        return pickle.load(file)

# Loading CSData
def LoadElectricalData():
    with open(ElectricalData, 'rb') as file:
        return pickle.load(file)

# Loading CSData
def LoadIndustrialData():
    with open(IndustrialData, 'rb') as file:
        return pickle.load(file)

# Loading CSData
def LoadMechanicalData():
    with open(MechanicalData, 'rb') as file:
        return pickle.load(file)
##########################################################################################



########################            Different Model Files              ########################################
#   Returns CS Model
def LoadCSModel(CSModelFile):
    loaded_model = pickle.load(open(CSModelFile, 'rb'))
    return loaded_model

#   Returns Electrical Model
def LoadElectricalModel(ElectricalModelFile):
    loaded_model = pickle.load(open(ElectricalModelFile, 'rb'))
    return loaded_model

#   Returns Industrial Model
def LoadIndustrialModel(IndustrialModelFile):
    loaded_model = pickle.load(open(IndustrialModelFile, 'rb'))
    return loaded_model

#   Returns  Mechanical Model
def LoadMechanicalModel(MechanicalModelFile):
    loaded_model = pickle.load(open(MechanicalModelFile, 'rb'))
    return loaded_model
##########################################################################################

#   To Select the  Features
features = ["TOEFLIELTSTest", "TOEFLIELTSOverall","TOEFLIELTSReading","TOEFLIELTSWriting","TOEFLIELTSListning", "TOEFLIELTSSpeaking","GREGMATOverall","Quant","Verbal","CPI", "CollegeAcademicOverall","ATKT","MajorSubject"]

#   Features to use in Prediction
PredictionFeatures = ["IELTS_Score", "GRE_Class","Quant_Class","Verbal_Class", "CPI_Class", "OverAll_Class","ATKT_Class","IELTS_Reading_Class","IELTS_Listning_Class","IELTS_Speaking_Class","IELTS_Writing_Class"]

#   For Getting Data by Labels
def ClusterIndicesNumpy(clustNum, labels_array):  # numpy
    return np.where(labels_array == clustNum)[0]

#   Convert TOEFL Scores into IELTS Scores
def IELTSConversion(Data,bin,level):
    i = 0
    while True:
        if bin[i]<=Data:
            i+=1
        else:
            break
    return level[i-1]

def DataBinning(Data):
    #   Using Binning Method to create classes for student's GRE Scores,CollegeOverall,CPI,
    GRE_bins = [258, 269, 279, 289, 299, 309, 319, 329, 340]
    GRE_Levels = [1, 2, 3, 4, 5, 6, 7, 8]

    CPI_bins = [-1, 2, 3, 4, 5, 6, 7, 8, 9, 11]
    CPI_Levels = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    OverAll_bins = [-1, 2, 3, 4, 5, 6, 7, 8, 9, 11]
    OverAll_Levels = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    Quant_Bins = [129, 135, 140, 145, 150, 155, 160, 165, 171]
    Quant_Levels = [1, 2, 3, 4, 5, 6, 7, 8]

    Verbal_Bins = [129, 136, 140, 146, 150, 156, 160, 166, 171]
    Verbal_Levels = [1, 2, 3, 4, 5, 6, 7, 8]

    TOEFL_bins = [-1, 31, 34, 45, 59, 78, 93, 101, 109, 114, 117, 121]
    TOEFL_Levels = [4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

    TOEFL_Reading = [-1, 2, 4, 8, 13, 15, 19, 24, 27, 28, 29, 31]
    TOEFL_Reading_Levels = [4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

    TOEFL_Writing = [-1, 6, 12, 14, 18, 21, 23, 24, 26, 28, 29, 31]
    TOEFL_Writing_Levels = [4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

    TOEFL_Listening = [-1, 2, 4, 7, 12, 16, 20, 24, 27, 28, 29, 31]
    TOEFL_Listening_Levels = [4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

    TOEFL_Speaking = [-1, 6, 12, 14, 16, 18, 20, 23, 24, 26, 28, 31]
    TOEFL_Speaking_Levels = [4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

    ATKT_bins = [-1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 101]
    ATKT_Levels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    Data['GRE_Class'] = pd.DataFrame(pd.cut(Data['GREGMATOverall'], GRE_bins, labels=GRE_Levels))

    Data['Quant_Class'] = pd.DataFrame(pd.cut(Data['Quant'], Quant_Bins, labels=Quant_Levels))
    Data['Verbal_Class'] = pd.DataFrame(pd.cut(Data['Verbal'], Verbal_Bins, labels=Verbal_Levels))

    Data['CPI_Class'] = pd.DataFrame(pd.cut(Data['CPI'], CPI_bins, labels=CPI_Levels))
    Data['OverAll_Class'] = pd.DataFrame(pd.cut(Data['CollegeAcademicOverall'], OverAll_bins, labels=OverAll_Levels))

    Data['ATKT_Class'] = pd.DataFrame(pd.cut(Data['ATKT'], ATKT_bins, labels=ATKT_Levels))

    Data['IELTS_Score'] = Data['TOEFLIELTSOverall']
    Data['IELTS_Reading_Class'] = Data['TOEFLIELTSReading']
    Data['IELTS_Listning_Class'] = Data['TOEFLIELTSListning']
    Data['IELTS_Writing_Class'] = Data['TOEFLIELTSWriting']
    Data['IELTS_Speaking_Class'] = Data['TOEFLIELTSSpeaking']

    for index, rows in Data.iterrows():
        if rows['TOEFLIELTSTest'] == 'TOEFL':
            Data.loc[index, 'IELTS_Score'] = IELTSConversion(rows['TOEFLIELTSOverall'], TOEFL_bins, TOEFL_Levels)
            Data.loc[index, 'IELTS_Reading_Class'] = IELTSConversion(rows['TOEFLIELTSReading'], TOEFL_Reading,
                                                                     TOEFL_Reading_Levels)
            Data.loc[index, 'IELTS_Listning_Class'] = IELTSConversion(rows['TOEFLIELTSListning'], TOEFL_Listening,
                                                                      TOEFL_Listening_Levels)
            Data.loc[index, 'IELTS_Speaking_Class'] = IELTSConversion(rows['TOEFLIELTSSpeaking'], TOEFL_Speaking,
                                                                      TOEFL_Speaking_Levels)
            Data.loc[index, 'IELTS_Writing_Class'] = IELTSConversion(rows['TOEFLIELTSWriting'], TOEFL_Writing,
                                                                     TOEFL_Writing_Levels)

    # print(Data)
    return Data

def TakeInput(df):
    #   Taking Input Data
    ToeflIeltsExam = input("Enter IELTS/TOEFL Exam : ")
    ToeflIeltsScore = float(input("Enter IELTS/TOEFL Score : "))
    ToeflIeltsReadingScore = float(input("Enter IELTS/TOEFL Reading Score : "))
    ToeflIeltsWritingScore = float(input("Enter IELTS/TOEFL Writing Score : "))
    ToeflIeltsListeningScore = float(input("Enter IELTS/TOEFL Listening Score : "))
    ToeflIeltsSpeakingScore = float(input("Enter IELTS/TOEFL Speaking Score : "))
    GREScore = int(input("Enter GRE Score : "))
    Quant = int(input("Enter GRE Quant Score : "))
    Verbal = int(input("Enter GRE Verbal Score : "))
    CPIScore = float(input("Enter CPI Score : "))
    CollegeScore = float(input("Enter College Overall  Score (Out of 10) : "))
    ATKTAll = int(input("Enter Total ATKTs : "))
    MajorSubject = input("Enter MajorSubject : ")
    df = pd.DataFrame(data=[[ToeflIeltsExam, ToeflIeltsScore,ToeflIeltsReadingScore,ToeflIeltsWritingScore,ToeflIeltsListeningScore,
                             ToeflIeltsSpeakingScore,GREScore,Quant,Verbal, CPIScore, CollegeScore, ATKTAll,MajorSubject]],
                      columns=features)
    return df





def PrintUniversities(GetData,Predicted_Value):
    for index, row in GetData.iterrows():
        if row['Labels'] == Predicted_Value:
             UniversityList.append(row["UniversityAppliedUniversityName"])
    print("\n".join(set(UniversityList)))


if __name__ == '__main__':

    #   For new Data Coming up, for prediction
    df = pd.DataFrame()

    Input = TakeInput(df)
    Binned_Input = DataBinning(Input)

    if Input["MajorSubject"].str.contains("Computer Science").any():
        CSModel = LoadCSModel(CSModelFile)
        Predicted_Value = CSModel.predict(Binned_Input[PredictionFeatures])
        GetCSLabeledData = LoadCSData()
        PrintUniversities(GetCSLabeledData, Predicted_Value)

        #   Does the job of Updating CSModel
        CSModel = CSModel.partial_fit(Binned_Input[PredictionFeatures].values)
        UpdateModel(CSModel,CSModelFile)

    elif Input["MajorSubject"].str.contains("Electrical Engineering").any():
        ElectricalModel = LoadElectricalModel(ElectricalModelFile)
        Predicted_Value = ElectricalModel.predict(Binned_Input[PredictionFeatures])
        GetElectricalLabeledData = LoadElectricalData()
        PrintUniversities(GetElectricalLabeledData,Predicted_Value)

    elif Input["MajorSubject"].str.contains("Industrial Engineering").any():
        IndustrialModel = LoadIndustrialModel(IndustrialModelFile)
        Predicted_Value = IndustrialModel.predict(Binned_Input[PredictionFeatures])
        GetIndustrialLabeledData = LoadIndustrialData()
        PrintUniversities(GetIndustrialLabeledData,Predicted_Value)

    elif Input["MajorSubject"].str.contains("Mechanical Engineering").any():
        MechanicalModel = LoadMechanicalModel(MechanicalModelFile)
        Predicted_Value = MechanicalModel.predict(Binned_Input[PredictionFeatures])
        GetMechanicalLabeledData = LoadMechanicalData()
        PrintUniversities(GetMechanicalLabeledData,Predicted_Value)