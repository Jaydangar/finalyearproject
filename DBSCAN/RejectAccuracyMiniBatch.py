import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt

from sklearn.cluster import KMeans
from pathlib import Path
from sklearn import metrics

#    For Printing of Data, All features are not shown Horizontally.
pd.set_option('display.height', 1000)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# save the model and Data to disk
ModelFile = Path('D:\BTP\MiniBatchCSTrainedData\SavedMiniBatchCSModel.pkl')
DataFile = Path('D:\BTP\MiniBatchCSTrainedData\SavedMiniBatchCSData.pkl')
ScoreFile = Path('D:\BTP\MiniBatchCSTrainedData\SavedMiniBatchClusterScore.pkl')
SaveTrainSet = Path('D:\BTP\MiniBatchCSTrainedData\SavedMiniBatchCSTrainingData.pkl')
SaveTestSet = Path('D:\BTP\MiniBatchCSTrainedData\SavedMiniBatchCSTestingData.pkl')

#   Accuracy = 0
global Train_Data,Test_Data,KMeansObj,Labeled_Data,ClusterScore

#   To Select the  Features
features = ["IELTS_Score", "GRE_Class","Quant_Class","Verbal_Class", "CPI_Class", "OverAll_Class", "ATKT_Class","IELTS_Reading_Class","IELTS_Listning_Class"
                ,"IELTS_Speaking_Class","IELTS_Writing_Class"]

#   For getting results for new entries
update_features = ['TOEFLIELTSTest', 'TOEFLIELTSOverall', 'GREGMATOverall', 'CPI', 'CollegeAcademicOverall', 'ATKT']

#   Using Binning Method to create classes for student's GRE Scores,CollegeOverall,CPI,
GRE_bins = [258, 269, 279, 289, 299, 309, 319, 329, 340]
GRE_Levels = [1, 2, 3, 4, 5, 6, 7, 8]

CPI_bins = [-1, 2, 3, 4, 5, 6, 7, 8, 9, 11]
CPI_Levels = [1, 2, 3, 4, 5, 6, 7, 8, 9]

OverAll_bins = [-1, 2, 3, 4, 5, 6, 7, 8, 9, 11]
OverAll_Levels = [1, 2, 3, 4, 5, 6, 7, 8, 9]

Quant_Bins = [129,135,140,145,150,155,160,165,171]
Quant_Levels = [1,2,3,4,5,6,7,8]

Verbal_Bins = [129,136,140,146,150,156,160,166,171]
Verbal_Levels = [1,2,3,4,5,6,7,8]

TOEFL_bins = [-1, 31, 34, 45, 59, 78, 93, 101, 109, 114, 117, 121]
TOEFL_Levels = [4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

TOEFL_Reading = [-1,2,4,8,13,15,19,24,27,28,29,31]
TOEFL_Reading_Levels = [4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9]

TOEFL_Writing = [-1,6,12,14,18,21,23,24,26,28,29,31]
TOEFL_Writing_Levels = [4,4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

TOEFL_Listening = [-1,2,4,7,12,16,20,24,27,28,29,31]
TOEFL_Listening_Levels = [4,4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

TOEFL_Speaking = [-1,6,12,14,16,18,20,23,24,26,28,31]
TOEFL_Speaking_Levels = [4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

ATKT_bins = [-1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 101]
ATKT_Levels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

#   University_bins = [0,100,200,300,400,500,600,700,800,900]
#   University_Levels = [1,2,3,4,5,6,7,8,9]

################################################
#                   For Saving and loading TrainingData                               #
################################################

def SaveTrainingData(DataVariable):
    with open(SaveTrainSet, 'wb+') as file:
        pickle.dump(DataVariable, file)

def LoadTrainingData():
    with open(SaveTrainSet,'rb') as file:
        return pickle.load(file)


################################################
#                   For Saving and loading TestingData                               #
################################################

def SaveTesingData(DataVariable):
    with open(SaveTestSet, 'wb+') as file:
        pickle.dump(DataVariable, file)

def LoadTestingData():
    with open(SaveTestSet,'rb') as file:
        return pickle.load(file)


################################################
#                  For Saving and loading Labeled Data                               #
################################################

def SaveData(DataVariable):
    with open(DataFile, 'wb+') as file:
        pickle.dump(DataVariable, file)

def LoadData():
    with open(DataFile,'rb') as file:
        return pickle.load(file)

################################################
#                   For Saving and loading silhouette_score                          #
################################################

def ScoreSave(ClusterScore):
    with open(ScoreFile, 'wb+') as file:
        pickle.dump(ClusterScore, file)

def ScoreLoad():
    with open(ScoreFile, 'rb') as file:
        return pickle.load(file)

################################################
#                                     Saving Model                                             #
################################################

def SaveModel(model):
    pickle.dump(model, open(ModelFile, 'wb+'))

#   Loading Saved Model
def LoadModel(ModelFile):
    loaded_model = pickle.load(open(ModelFile, 'rb'))
    return loaded_model


#   For Getting Data by Labels
def ClusterIndicesNumpy(clustNum, labels_array):  # numpy
    return np.where(labels_array == clustNum)[0]

#   Convert TOEFL Scores into IELTS Scores
def IELTSConversion(Data,bin,level):
    i = 0
    while True:
        if bin[i]<=Data:
            i+=1
        else:
            break
    return level[i-1]

def DataBinning(Data):

    Data['GRE_Class'] = pd.DataFrame(pd.cut(Data['GREGMATOverall'], GRE_bins, labels=GRE_Levels))

    Data['Quant_Class'] = pd.DataFrame(pd.cut(Data['Quant'], Quant_Bins, labels=Quant_Levels))
    Data['Verbal_Class'] = pd.DataFrame(pd.cut(Data['Verbal'], Verbal_Bins, labels=Verbal_Levels))

    Data['CPI_Class'] = pd.DataFrame(pd.cut(Data['CPI'], CPI_bins, labels=CPI_Levels))
    Data['OverAll_Class'] = pd.DataFrame(pd.cut(Data['CollegeAcademicOverall'], OverAll_bins, labels=OverAll_Levels))

    Data['ATKT_Class'] = pd.DataFrame(pd.cut(Data['ATKT'], ATKT_bins, labels=ATKT_Levels))

    Data['IELTS_Score'] = Data['TOEFLIELTSOverall']
    Data['IELTS_Reading_Class'] = Data['TOEFLIELTSReading']
    Data['IELTS_Listning_Class'] = Data['TOEFLIELTSListning']
    Data['IELTS_Writing_Class'] = Data['TOEFLIELTSWriting']
    Data['IELTS_Speaking_Class'] = Data['TOEFLIELTSSpeaking']

    for index,rows in Data.iterrows():
        if rows['TOEFLIELTSTest'] == 'TOEFL':
            Data.loc[index,'IELTS_Score']=IELTSConversion(rows['TOEFLIELTSOverall'],TOEFL_bins,TOEFL_Levels)
            Data.loc[index, 'IELTS_Reading_Class'] = IELTSConversion(rows['TOEFLIELTSReading'], TOEFL_Reading, TOEFL_Reading_Levels)
            Data.loc[index, 'IELTS_Listning_Class'] = IELTSConversion(rows['TOEFLIELTSListning'], TOEFL_Listening, TOEFL_Listening_Levels)
            Data.loc[index, 'IELTS_Speaking_Class'] = IELTSConversion(rows['TOEFLIELTSSpeaking'], TOEFL_Speaking, TOEFL_Speaking_Levels)
            Data.loc[index, 'IELTS_Writing_Class'] = IELTSConversion(rows['TOEFLIELTSWriting'], TOEFL_Writing, TOEFL_Writing_Levels)

    #   print(Data)

    #   Selecting rows having Computer Science as Major Subject
    Data = Data.loc[Data["MajorSubject"] == 'Computer Science']

    return Data


def TestDataBinning(Data):
    Data['GRE_Class'] = pd.DataFrame(pd.cut(Data['GREGMATOverall'], GRE_bins, labels=GRE_Levels))

    Data['Quant_Class'] = pd.DataFrame(pd.cut(Data['Quant'], Quant_Bins, labels=Quant_Levels))
    Data['Verbal_Class'] = pd.DataFrame(pd.cut(Data['Verbal'], Verbal_Bins, labels=Verbal_Levels))

    Data['CPI_Class'] = pd.DataFrame(pd.cut(Data['CPI'], CPI_bins, labels=CPI_Levels))
    Data['OverAll_Class'] = pd.DataFrame(pd.cut(Data['CollegeAcademicOverall'], OverAll_bins, labels=OverAll_Levels))

    Data['ATKT_Class'] = pd.DataFrame(pd.cut(Data['ATKT'], ATKT_bins, labels=ATKT_Levels))

    Data['IELTS_Score'] = Data['TOEFLIELTSOverall']
    Data['IELTS_Reading_Class'] = Data['TOEFLIELTSReading']
    Data['IELTS_Listning_Class'] = Data['TOEFLIELTSListning']
    Data['IELTS_Writing_Class'] = Data['TOEFLIELTSWriting']
    Data['IELTS_Speaking_Class'] = Data['TOEFLIELTSSpeaking']

    #   Converting TOEFL Scores into IELTS Score
    for index, rows in Data.iterrows():
        if rows['TOEFLIELTSTest'] == 'TOEFL':
            Data.loc[index, 'IELTS_Score'] = IELTSConversion(rows['TOEFLIELTSOverall'], TOEFL_bins, TOEFL_Levels)
            Data.loc[index, 'IELTS_Reading_Class'] = IELTSConversion(rows['TOEFLIELTSReading'], TOEFL_Reading,TOEFL_Reading_Levels)
            Data.loc[index, 'IELTS_Listning_Class'] = IELTSConversion(rows['TOEFLIELTSListning'], TOEFL_Listening,TOEFL_Listening_Levels)
            Data.loc[index, 'IELTS_Speaking_Class'] = IELTSConversion(rows['TOEFLIELTSSpeaking'], TOEFL_Speaking,TOEFL_Speaking_Levels)
            Data.loc[index, 'IELTS_Writing_Class'] = IELTSConversion(rows['TOEFLIELTSWriting'], TOEFL_Writing,TOEFL_Writing_Levels)

    return Data

def Clustring(Data, NoOfClusters):
    UniqueUniversities = set(Data["UniversityAppliedUniversityName"])
    # print(len(UniqueUniversities))

    #   To store All cluster's number for individual Universities
    ClusterList = {}

    #   To store cluster's number for individual Universities
    Clusters = []

    #   For preventing to print all cluster info
    j = 0

    #   Getting label wise rows
    for UniversityName in UniqueUniversities:
        #   print(UniversityName)
        for i in range(NoOfClusters):
            #   print("Cluster Number is : ",i)
            #   Getting Unique Universities Cluster wise
            GetUniversities = Data[Data['Labels'] == i]["UniversityAppliedUniversityName"]
            if j < NoOfClusters:
                 print("\n\n")
                 print("Cluster Number : " + str(i) + " size is :  ", len(set(GetUniversities)))
                 print("\n".join(set(GetUniversities)))
                 j += 1
            if UniversityName in set(GetUniversities):
                Clusters.append(i)

                # print(UniversityName, Clusters, sep='\t\t')
        ClusterList[UniversityName] = list(Clusters)
        Clusters.clear()
    return ClusterList

def GetBinnedData():
    #   Imporing Dataset
    ALL_Data = pd.read_csv('D:\BTP\REJECT_TEST_ENTRIES.csv')

    #   Remove colomns containing NaN values in any colomns
    ALL_Data.dropna(how='any')

    #   Binning Of Data -->  Adds New colomns that contains bin value of each samples
    #   Returns CS Binned Data only
    BinnedData = DataBinning(ALL_Data)

    return BinnedData

    #   print(BinnedData)

def PrintUniversities(Data,NoOfClusters):
    #   To store cluster's number for individual Universities
    Clusterlist = []

    #   For preventing to print all cluster info
    j = 0

    #   Getting label wise rows
    for UniversityName in UniqueUniversities:
        #   print(UniversityName)
        i = 0
        while i < NoOfClusters:
            #   print("Cluster Number is : ",i)
            #   Getting Unique Universities Cluster wise
            GetUniversities = Data[Data['Labels'] == i]["UniversityAppliedUniversityName"]

            if j < NoOfClusters:

                print("\n\n")

                print("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
                print("Cluster Number : " + str(i) + " size is :  ", len(set(GetUniversities)))
                print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n")

                print("##############################################")
                print("\n".join(set(GetUniversities)))
                print("##############################################")

                j += 1

            if UniversityName in set(GetUniversities):
                Clusterlist.append(i)
            i += 1

        #   print(UniversityName, Clusterlist, sep='\t\t')
        Clusterlist.clear()


if __name__ == '__main__':

    BinnedData = GetBinnedData()

    Test_Data = BinnedData
    #   print('Size of Test Data is  :  ', len(Test_Data))

    # #   Calling main Function --> Adds New colomn that contains cluster value for each sample
    # Labeled_Data, KMeansObj,ClusterScore = TrainModel(Train_Data, NoOfClusters)

    #   For Evaluating Cluster Performance
    Dictionary = {}
    ClusterScore = {}
    Accuracy = {}

    UniqueUniversities = pd.Series.unique(Test_Data["UniversityAppliedUniversityName"])

    print("###############################################")
    print("\t No Of Clusters = ",20)
    print("###############################################")

    MiniBatchModel = LoadModel(ModelFile)
    labels = MiniBatchModel.labels_

    #   Adding Cluster Label Colomn
    Labeled_CS_Data = LoadData()

    PrintUniversities(Labeled_CS_Data,20)

    #   Creates Dictionaries containing University Name as key and Clusters in which a University resides
    #   GetDictionary = Clustring(Labeled_CS_Data, NoOfClusters)

    Predicted_Value = MiniBatchModel.predict(Test_Data[features])

    #   count = 0
    TrueCount = 0

    #   Getting DataFrame, Indexing starting from 0
    GetTestData = Test_Data.reset_index(drop=True)
    #   print(GetTestData)

    WrongList = []
    WrongCount = 0

    #   Iterate through Each Rows
    #   index  is index starts from 0
    #   i is Cluster Number
    for index, i in np.ndenumerate(Predicted_Value):
        #   Taking Unique Universities in each clusters
        GetUniversities = set(Labeled_CS_Data[Labeled_CS_Data['Labels'] == i]["UniversityAppliedUniversityName"])
        #   print(i,GetUniversities)
        #   If "Suggested" University is in our cluster
        if GetUniversities.intersection(GetUniversities, GetTestData.loc[[i]]["UniversityAppliedUniversityName"]):
            WrongCount += 1
            #   print(GetUniversities)
            #   print(GetTestData.loc[[i]]["UniversityAppliedUniversityName"])
            WrongList.append(i)
        else:
            TrueCount += 1
            #   print(TrueCount)

    print("-------------------- Accuracy mesure of a Cluster -------------------")
    # print(len(GetList.drop_duplicates(subset=features, keep='first')))
    print("TrueCount ", TrueCount)
    print("WrongCount ", WrongCount)
    print('Length of Test Dataset : ', len(GetTestData))
    print('Wrong Clusters : ', set(WrongList))
    print('Total Wrong Entries in Test Data : ', len(set(WrongList)))

    print("-------------------- Accuracy in percentage -------------------")
    Accuracy = (len(GetTestData) - WrongCount) * 100 / (len(GetTestData))
    print('Cluster Accuracy is : ', Accuracy)

    print('Cluster Performance is : ')
    print(ClusterScore)

    print("#########    END     ############")
    print("==========================================================================")
    print("==========================================================================")
    print("==========================================================================")
    print("==========================================================================")
    print("==========================================================================")
    print("\n\n")