# Refered Paper link : http://www.eecs.tufts.edu/~dsculley/papers/fastkmeans.pdf 
# Algorithm 1.
from __future__ import division
from sklearn.cluster import MiniBatchKMeans
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pickle

def write_module(module,filename):
	outputFile = open(filename,'wb')
	s=pickle.dumps(module)
	outputFile.write(s)
	outputFile.close()

def read_module(filename):
	input_data = open(filename,'r')
	stru = input_data.read()
	k_module = pickle.loads(stru)
	return k_module


def intial_update(data,no_clusters,iterations,filename):
	#skill = np.load(data_file)
	np.random.shuffle(data)
	kmeans = MiniBatchKMeans(n_clusters=no_clusters,max_iter=iterations)
	np.random.shuffle(data)
	row,column = data.shape
	batch=500

	for i in range(0,row,batch):
		if (i+batch)<=row:
			kmeans.partial_fit(data[i:i+batch])
		else:
			kmeans.partial_fit(data[i:row])
	write_module(kmeans,filename)		


def partial_update(data,filename):
	kmeans = read_module(filename)
	np.random.shuffle(data)
	kmeans.partial_fit(data)
	write_module(kmeans,filename)

# Return average distance from repsected centroid.
def avg_distance(centroid,data,labels):
	total=0
	for i in range(len(labels)):
		total+=np.linalg.norm(data[i]-centroid[labels[i]])
	return total/len(labels)



no_clusters= 400 
iterations = 1000

filename="Kmeans_clustering.pkl"
#print type(s)
#labels = kmeans.predict(skill)
#centroids = kmeans.cluster_centers_

data = np.load("skill_features.npy")
np.random.shuffle(data)
train_data_intial=data[:8900,:]
train_data_partial=data[8900:9001,:]
test_data=data[9001:,:]
intial_update(train_data_intial,no_clusters,iterations,filename)
partial_update(train_data_partial,filename)

test_module = read_module(filename)
centroids = test_module.cluster_centers_
labels = test_module.predict(data)
train_labels=labels[:9001]
test_labels=labels[9001:]

print avg_distance(centroids,data,labels)
# No of clusters     Avg.Distance       
#	6					2.1989	
#	5					2.2539
#	4					2.2760
#	3					2.2974
#	2					2.3163
#	1					2.38107

#dist = np.linalg.norm(a-b)

# Printing skills of vectors within same cluster.
# b=np.load("skill_dict.npy")
# for j in range(0,1000):
# 	if train_labels[j]==test_labels[0]:
# 		for i in range(len(data[j])):
# 			if data[j][i]==1:
# 				print(b[i],end=" ")
# 		print("\n")
# 		for i in range(len(data[j])):
# 			if data[9001][i]==1:
# 				print(b[i],end=" ")
# 		print("\n","##################")

#print centroids
#print labels
# fig = plt.figure(figsize=(5, 5))
# colmap = {1: 'r', 2: 'g', 3: 'b'}
# colors = map(lambda x: colmap[x+1], labels)
# print skill[:,1]
# plt.scatter(skill[:,1], skill[:,2000], color=colors, alpha=0.5, edgecolor='k')
# for idx, centroid in enumerate(centroids[:,[1,2]]):
#     plt.scatter(*centroid, color=colmap[idx+1])
# plt.show()


#test = pickle.loads(s)
#print("label:",test.predict(skill))
#print("centroid",test.cluster_centers_)