import pandas as pd
import numpy as np
from sklearn.cluster import KMeans

#   For Getting Data by Labels
def ClusterIndicesNumpy(clustNum, labels_array): #numpy
    return np.where(labels_array == clustNum)[0]

#    For Printing of Data, All features are not shown Horizontally.
pd.set_option('display.height', 1000)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

Data = pd.read_csv('D:\BTP\Cluster_Data.csv')

#   Setting Number of Clusters
NoOfClusters = 30

#   Using Binning Method to create classes for student's GRE Scores,CollegeOverall,CPI,
GRE_bins = [259, 269, 279, 289, 299, 309, 319, 329, 339]
GRE_Levels = [1, 2, 3, 4, 5, 6, 7, 8]

CPI_bins = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
CPI_Levels = [1, 2, 3, 4, 5, 6, 7, 8, 9]

OverAll_bins = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
OverAll_Levels = [1, 2, 3, 4, 5, 6, 7, 8, 9]

TOEFL_bins = [0, 31, 34, 45, 59, 78, 93, 101, 109, 114, 117, 120]
TOEFL_Levels = [4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

Data['GRE_Class'] = pd.DataFrame(pd.cut(Data['GREGMATOverall'], GRE_bins, labels=GRE_Levels))
Data['CPI_Class'] = pd.DataFrame(pd.cut(Data['CPI'], CPI_bins, labels=CPI_Levels))
Data['OverAll_Class'] = pd.DataFrame(pd.cut(Data['CollegeAcademicOverall'], OverAll_bins, labels=OverAll_Levels))

# If Student has given TOEFL then convert that score into IELTS Score and Create new Colomn IELTS_Score for that
TOEFL_Rows = Data.loc[Data['TOEFLIELTSTest'] == 'TOEFL']
Data['IELTS_Score'] = pd.DataFrame(pd.cut(TOEFL_Rows['TOEFLIELTSOverall'], TOEFL_bins, labels=TOEFL_Levels))
Data['IELTS_Score'] = pd.DataFrame(pd.np.where(Data['TOEFLIELTSTest'] == 'IELTS', Data['TOEFLIELTSOverall'], Data['IELTS_Score']))

#   Selecting rows having Computer Science as Major Subject
CS_Data = Data.loc[Data["MajorSubject"] == "Computer Science"]

#   To Select the  Features
features = ["IELTS_Score", "GRE_Class", "CPI_Class", "OverAll_Class", "ATKT"]

#   Taking mean of every colomns
#   AppliedUniversityMean = pd.DataFrame(Data.groupby("UniversityAppliedUniversityName")[features].mean())

#   Taking the mean value for each attributes for each Applied University
#   Get Square values of each colomns in python

#   AppliedUniversityMean = CS_Data.groupby("UniversityAppliedUniversityName")
#   AppliedUniversityMean['MeanSum'] = pd.DataFrame(AppliedUniversityMean[features].sum(axis=1))

kmeans = KMeans(n_clusters=NoOfClusters, init='k-means++').fit(CS_Data[features])
labels = kmeans.labels_

#   Adding Cluster Label Colomn
CS_Data = CS_Data.assign(Labels=pd.Series(labels).values)

UniqueUniversities = pd.Series.unique(CS_Data["UniversityAppliedUniversityName"])
#print(len(UniqueUniversities))

#   To store cluster's number for individual Universities
Clusterlist = []

#   For preventing to print all cluster info
j = 0

#   Getting label wise rows
for UniversityName in UniqueUniversities:
    #   print(UniversityName)
    i = 0
    while i < NoOfClusters:
        #   print("Cluster Number is : ",i)
        #   Getting Unique Universities Cluster wise
        GetUniversities = CS_Data[CS_Data['Labels']==i]["UniversityAppliedUniversityName"]

        if j<30:
            print("Cluster Number : " + str(i) + " size is :  ", len(set(GetUniversities)))
            print(set(GetUniversities))
            j+=1

        if UniversityName in set(GetUniversities):
            Clusterlist.append(i)
        i += 1

    #print(UniversityName, Clusterlist, sep='\t\t')
    Clusterlist.clear()

'''
#   To get list of clusters in which University belongs to 
###########     start    ##############
 if UniversityName in set(GetUniversities):
            Clusterlist.append(i)
        i += 1

    print(UniversityName,Clusterlist,sep='\t\t')
    Clusterlist.clear()
#############   end    ###################

#############This is Different###########
for LabelNumber in uniquevalues:
    newdf = CS_Data[CS_Data['LabelNumber'] == LabelNumber]
    print(newdf)
'''

# Predicting for input Data
Input = [[4,5,5,5,0]]
print(kmeans.predict(Input))