import pandas as pd
import numpy as np

from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.metrics import confusion_matrix
from sklearn.neighbors import KNeighborsClassifier

data = pd.read_csv('D:\BTP\Toefl_Overall.csv')

# Split the data into a training set and a test set
#  Split is 80% training and 20% validation

Train_Data, Validate_Data, Test_Data = np.split(data.sample(frac=1), [int(.7*len(data)), int(.9*len(data))])

#print Train_Data
print 'Number of Training Samples : ', len(Train_Data)
print 'Number of Validation Samples : ',len(Validate_Data)
print 'Number of Test Samples : ', len(Test_Data)

#   TF_Data = Training Feature Data
#   TO_Data = Training Output Label
TF_Data = Train_Data[['CollegeAcademicOverall','CPI','ATKT','GREGMATOverall','Quant','Verbal',
                                'Analy','TOEFLIELTSOverall','TOEFLIELTSReading','TOEFLIELTSListning',
                                'TOEFLIELTSSpeaking','TOEFLIELTSWriting']]

TO_Data = Train_Data['UniversityAppliedUniversityName']

# For Validation Data
Validate_TF_Data = Validate_Data[['CollegeAcademicOverall','CPI','ATKT','GREGMATOverall','Quant','Verbal',
                                'Analy','TOEFLIELTSOverall','TOEFLIELTSReading','TOEFLIELTSListning',
                                'TOEFLIELTSSpeaking','TOEFLIELTSWriting']]

Validate_TO_Data = Validate_Data['UniversityAppliedUniversityName']

'''
print 'Training Data is : ',TF_Data
print 'Training Output Data is : ',TO_Data
print 'Validation Input Data : ',Validate_TF_Data
print 'Validation Output Data : ',Validate_TO_Data

print('\nFeature Data : \n')
print (TF_Data.head(5))
print('\nOutput Data : \n')
print(TO_Data.head(5))
'''

logistic = LogisticRegression(solver='newton-cg',multi_class='multinomial',max_iter=1000)
logistic.fit(TF_Data,TO_Data)

SVC  =  LinearSVC(multi_class='crammer_singer')
SVC.fit(TF_Data,TO_Data)

Knn = KNeighborsClassifier(algorithm='auto')
Knn.fit(TF_Data,TO_Data)
'''
#   testing In Process Input
X = [[7.38,7.38,0,306,160,146,3,82,21,20,19,22],
[6.32,7.02,5,303,160,143,3,93,20,26,24,23],
[7.68,7.68,0,300,158,142,3,80,18,17,24,21],
[6.34,6.48,0,315,166,149,3,92,24,20,20,28],
[8.03,8.03,0,300,156,144,3.5,79,17,16,24,22],
[6.04,6.04,0,298,157,141,3,81,18,22,18,23],
[5.9,6.47,0,286,145,141,1.5,61,12,20,15,14],
[7.5,7.5,0,304,24,20,3,101,26,28,23,24],
[6.54,7.04,3,290,154,136,3,90,19,29,22,20],
[5.033,5.033,0,290,154,136,3,90,19,29,22,20],
[7.35,7.92,0,306,158,148,4,112,28,29,26,29],
[7.97,8.59,2,301,159,142,2,73,23,20,15,15],
[7.97,8.59,2,301,159,142,2,79,22,23,18,16],
[7.55,7.78,0,301,160,141,2.5,74,18,18,20,18],
[6.18,6.08,0,307,158,149,3,102,27,26,24,25],
[6.6,7.04,13,292,154,138,2,72,15,22,18,17],
[7.49,7.49,0,308,161,147,3,102,22,27,24,29],
[5.51,5.51,0,309,158,151,4.5,110,29,30,23,28],
[6.78,6.78,0,309,158,151,4.5,110,29,30,23,28],
[6.74,7.48,0,313,168,145,3.5,94,22,24,26,22],
[6.93,6.93,0,303,163,140,2,81,18,25,22,16],
[7.72,7.72,0,306,161,145,3.5,99,26,24,24,25],
[6.325,6.325,0,311,161,150,3,101,29,27,20,25],
[6.566,6.566,0,317,165,152,3,94,26,21,23,24],
[6.79,6.97,5,296,155,141,2.5,77,15,18,20,24],
[6.79,6.97,5,296,155,141,2.5,77,15,18,20,24]]
'''

Regresison_Predicted_Output =  logistic.predict(Validate_TF_Data)
SVC_Predicted_Output = SVC.predict(Validate_TF_Data)
Knn_Predicted_Output = Knn.predict(Validate_TF_Data)

print 'Confusion Matrix Regression is : \n'
print confusion_matrix(Validate_TO_Data,Regresison_Predicted_Output)

print 'Confusion Matrix SVC is : \n'
print confusion_matrix(Validate_TO_Data,SVC_Predicted_Output)

print 'Confusion Matrix KNN is : \n'
print confusion_matrix(Validate_TO_Data,Knn_Predicted_Output)

# Printing Probability Value for each class
#print logistic.predict_proba(X)
'''
    State University of New York  Albany = 0
    New York Institute of Technology = 1
    New Jersey Institute of Technology = 2
'''