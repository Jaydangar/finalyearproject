import pandas as pd
import numpy as np
import pickle

from sklearn.cluster import MiniBatchKMeans
from pathlib import Path
from sklearn import metrics

#    For Printing of Data, All features are not shown Horizontally.
pd.set_option('display.height', 1000)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# save the model and Data to disk
ModelFile = Path('SavedMiniBatchModel.pkl')
DataFile = Path('SavedMiniBatchData.pkl')
ScoreFile = Path('SavedMiniBatchClusterScoreData.pkl')
SaveTrainSet = Path('SavedMiniBatchTrainingData.pkl')
SaveTestSet = Path('SavedMiniBatchTestingData.pkl')

#   To Select the  Features
features = ["IELTS_Score", "GRE_Class","Quant_Class","Verbal_Class", "CPI_Class", "OverAll_Class", "ATKT_Class","IELTS_Reading_Class","IELTS_Listning_Class"
                ,"IELTS_Speaking_Class","IELTS_Writing_Class"]

#   Print Features
PrintFeatures = ['GREGMATOverall','Quant','Verbal','Analy',
                        'TOEFLIELTSTest','TOEFLIELTSOverall','TOEFLIELTSReading','TOEFLIELTSListning',
                 'TOEFLIELTSSpeaking','TOEFLIELTSWriting','UniversityAppliedUniversityName',"IELTS_Score", "GRE_Class","Quant_Class","Verbal_Class", "CPI_Class", "OverAll_Class", "ATKT_Class","IELTS_Reading_Class","IELTS_Listning_Class"
                ,"IELTS_Speaking_Class","IELTS_Writing_Class"]

#   For getting results for new entries
update_features = ['TOEFLIELTSTest', 'TOEFLIELTSOverall', 'GREGMATOverall', 'CPI', 'CollegeAcademicOverall', 'ATKT']

#   Binning

#   Using Binning Method to create classes for student's GRE Scores,CollegeOverall,CPI,
GRE_bins = [258, 269, 279, 289, 299, 309, 319, 329, 340]
GRE_Levels = [1, 2, 3, 4, 5, 6, 7, 8]

CPI_bins = [-1, 2, 3, 4, 5, 6, 7, 8, 9, 11]
CPI_Levels = [1, 2, 3, 4, 5, 6, 7, 8, 9]

OverAll_bins = [-1, 2, 3, 4, 5, 6, 7, 8, 9, 11]
OverAll_Levels = [1, 2, 3, 4, 5, 6, 7, 8, 9]

Quant_Bins = [129,135,140,145,150,155,160,165,171]
Quant_Levels = [1,2,3,4,5,6,7,8]

Verbal_Bins = [129,136,140,146,150,156,160,166,171]
Verbal_Levels = [1,2,3,4,5,6,7,8]

TOEFL_bins = [-1, 31, 34, 45, 59, 78, 93, 101, 109, 114, 117, 121]
TOEFL_Levels = [4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

TOEFL_Reading = [-1,2,4,8,13,15,19,24,27,28,29,31]
TOEFL_Reading_Levels = [4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9]

TOEFL_Writing = [-1,6,12,14,18,21,23,24,26,28,29,31]
TOEFL_Writing_Levels = [4,4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

TOEFL_Listening = [-1,2,4,7,12,16,20,24,27,28,29,31]
TOEFL_Listening_Levels = [4,4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

TOEFL_Speaking = [-1,6,12,14,16,18,20,23,24,26,28,31]
TOEFL_Speaking_Levels = [4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9]

ATKT_bins = [-1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 101]
ATKT_Levels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

################################################
#                   For Saving and loading TrainingData                               #
################################################

def SaveTrainingData(DataVariable):
    with open(SaveTrainSet, 'wb') as file:
        pickle.dump(DataVariable, file)

def LoadTrainingData():
    with open(SaveTrainSet,'rb') as file:
        return pickle.load(file)


################################################
#                   For Saving and loading TestingData                               #
################################################

def SaveTesingData(DataVariable):
    with open(SaveTestSet, 'wb') as file:
        pickle.dump(DataVariable, file)

def LoadTestingData():
    with open(SaveTestSet,'rb') as file:
        return pickle.load(file)


################################################
#                  For Saving and loading Labeled Data                               #
################################################

def SaveData(DataVariable):
    with open(DataFile, 'wb') as file:
        pickle.dump(DataVariable, file)

def LoadData():
    with open(DataFile,'rb') as file:
        return pickle.load(file)

################################################
#                   For Saving and loading silhouette_score                          #
################################################

def ScoreSave(ClusterScore):
    with open(ScoreFile, 'wb') as file:
        pickle.dump(ClusterScore, file)

def ScoreLoad():
    with open(ScoreFile, 'rb') as file:
        return pickle.load(file)

################################################
#                                     Saving Model                                             #
################################################

def SaveModel(model):
    pickle.dump(model, open(ModelFile, 'wb'))

#   Loading Saved Model
def LoadModel(ModelFile):
    loaded_model = pickle.load(open(ModelFile, 'rb'))
    return loaded_model


#   For Getting Data by Labels
def ClusterIndicesNumpy(clustNum, labels_array):  # numpy
    return np.where(labels_array == clustNum)[0]

#   Convert TOEFL Scores into IELTS Scores
def IELTSConversion(Data,bin,level):
    i = 0
    while True:
        if bin[i]<=Data:
            i+=1
        else:
            break
    return level[i-1]

def DataBinning(Data):

    Data['GRE_Class'] = pd.DataFrame(pd.cut(Data['GREGMATOverall'], GRE_bins, labels=GRE_Levels))

    Data['Quant_Class'] = pd.DataFrame(pd.cut(Data['Quant'], Quant_Bins, labels=Quant_Levels))
    Data['Verbal_Class'] = pd.DataFrame(pd.cut(Data['Verbal'], Verbal_Bins, labels=Verbal_Levels))

    Data['CPI_Class'] = pd.DataFrame(pd.cut(Data['CPI'], CPI_bins, labels=CPI_Levels))
    Data['OverAll_Class'] = pd.DataFrame(pd.cut(Data['CollegeAcademicOverall'], OverAll_bins, labels=OverAll_Levels))

    Data['ATKT_Class'] = pd.DataFrame(pd.cut(Data['ATKT'], ATKT_bins, labels=ATKT_Levels))

    Data['IELTS_Score']=Data['TOEFLIELTSOverall']
    Data['IELTS_Reading_Class'] = Data['TOEFLIELTSReading']
    Data['IELTS_Listning_Class'] = Data['TOEFLIELTSListning']
    Data['IELTS_Writing_Class'] = Data['TOEFLIELTSWriting']
    Data['IELTS_Speaking_Class'] = Data['TOEFLIELTSSpeaking']

    for index,rows in Data.iterrows():
        if rows['TOEFLIELTSTest'] == 'TOEFL':
            Data.loc[index,'IELTS_Score']=IELTSConversion(rows['TOEFLIELTSOverall'],TOEFL_bins,TOEFL_Levels)
            Data.loc[index, 'IELTS_Reading_Class'] = IELTSConversion(rows['TOEFLIELTSReading'], TOEFL_Reading, TOEFL_Reading_Levels)
            Data.loc[index, 'IELTS_Listning_Class'] = IELTSConversion(rows['TOEFLIELTSListning'], TOEFL_Listening, TOEFL_Listening_Levels)
            Data.loc[index, 'IELTS_Speaking_Class'] = IELTSConversion(rows['TOEFLIELTSSpeaking'], TOEFL_Speaking, TOEFL_Speaking_Levels)
            Data.loc[index, 'IELTS_Writing_Class'] = IELTSConversion(rows['TOEFLIELTSWriting'], TOEFL_Writing, TOEFL_Writing_Levels)

    #   print(Data)

    #   Selecting rows having Computer Science as Major Subject
    CS_Data = Data.loc[Data["MajorSubject"] == 'Computer Science']

    return CS_Data


def TestDataBinning(Data):
    Data['GRE_Class'] = pd.DataFrame(pd.cut(Data['GREGMATOverall'], GRE_bins, labels=GRE_Levels))

    Data['Quant_Class'] = pd.DataFrame(pd.cut(Data['Quant'], Quant_Bins, labels=Quant_Levels))
    Data['Verbal_Class'] = pd.DataFrame(pd.cut(Data['Verbal'], Verbal_Bins, labels=Verbal_Levels))

    Data['CPI_Class'] = pd.DataFrame(pd.cut(Data['CPI'], CPI_bins, labels=CPI_Levels))
    Data['OverAll_Class'] = pd.DataFrame(pd.cut(Data['CollegeAcademicOverall'], OverAll_bins, labels=OverAll_Levels))

    Data['ATKT_Class'] = pd.DataFrame(pd.cut(Data['ATKT'], ATKT_bins, labels=ATKT_Levels))

    Data['IELTS_Score'] = Data['TOEFLIELTSOverall']
    Data['IELTS_Reading_Class'] = Data['TOEFLIELTSReading']
    Data['IELTS_Listning_Class'] = Data['TOEFLIELTSListning']
    Data['IELTS_Writing_Class'] = Data['TOEFLIELTSWriting']
    Data['IELTS_Speaking_Class'] = Data['TOEFLIELTSSpeaking']

    #   Converting TOEFL Scores into IELTS Score
    for index, rows in Data.iterrows():
        if rows['TOEFLIELTSTest'] == 'TOEFL':
            Data.loc[index, 'IELTS_Score'] = IELTSConversion(rows['TOEFLIELTSOverall'], TOEFL_bins, TOEFL_Levels)
            Data.loc[index, 'IELTS_Reading_Class'] = IELTSConversion(rows['TOEFLIELTSReading'], TOEFL_Reading,TOEFL_Reading_Levels)
            Data.loc[index, 'IELTS_Listning_Class'] = IELTSConversion(rows['TOEFLIELTSListning'], TOEFL_Listening,TOEFL_Listening_Levels)
            Data.loc[index, 'IELTS_Speaking_Class'] = IELTSConversion(rows['TOEFLIELTSSpeaking'], TOEFL_Speaking,TOEFL_Speaking_Levels)
            Data.loc[index, 'IELTS_Writing_Class'] = IELTSConversion(rows['TOEFLIELTSWriting'], TOEFL_Writing,TOEFL_Writing_Levels)

    # TOEFL_Rows = Data.loc[Data['TOEFLIELTSTest'] == 'TOEFL']
    # Data['IELTS_Score'] = pd.DataFrame(pd.cut(TOEFL_Rows['TOEFLIELTSOverall'], TOEFL_bins, labels=TOEFL_Levels))
    # Data['IELTS_Reading_Class'] = pd.DataFrame(pd.cut(TOEFL_Rows['TOEFLIELTSReading'], TOEFL_Reading, labels=TOEFL_Reading_Levels))
    # Data['IELTS_Listning_Class'] = pd.DataFrame(pd.cut(TOEFL_Rows['TOEFLIELTSListning'], TOEFL_Listening, labels=TOEFL_Listening_Levels))
    # Data['IELTS_Speaking_Class'] = pd.DataFrame(pd.cut(TOEFL_Rows['TOEFLIELTSSpeaking'], TOEFL_Speaking, labels=TOEFL_Speaking_Levels))
    # Data['IELTS_Writing_Class'] = pd.DataFrame(pd.cut(TOEFL_Rows['TOEFLIELTSWriting'], TOEFL_Writing, labels=TOEFL_Writing_Levels))
    #
    # IELTS_Rows = Data.loc[Data['TOEFLIELTSTest'] == 'IELTS']
    # Data['IELTS_Score'] = pd.DataFrame(IELTS_Rows['TOEFLIELTSOverall'])
    # Data['IELTS_Reading_Class'] = pd.DataFrame(IELTS_Rows['TOEFLIELTSReading'])
    # Data['IELTS_Listning_Class'] = pd.DataFrame(IELTS_Rows['TOEFLIELTSListning'])
    # Data['IELTS_Speaking_Class'] = pd.DataFrame(IELTS_Rows['TOEFLIELTSSpeaking'])
    # Data['IELTS_Writing_Class'] = pd.DataFrame(IELTS_Rows['TOEFLIELTSWriting'])

    return Data


def TrainModel(Data, NoOfClusters):
    #   Taking mean of every colomns
    #   AppliedUniversityMean = pd.DataFrame(Data.groupby("UniversityAppliedUniversityName")[features].mean())

    #   Taking the mean value for each attributes for each Applied University
    #   Get Square values of each colomns in python

    #   AppliedUniversityMean = CS_Data.groupby("UniversityAppliedUniversityName")
    #   AppliedUniversityMean['MeanSum'] = pd.DataFrame(AppliedUniversityMean[features].sum(axis=1))

    kmeans = KMeans(n_clusters=NoOfClusters,init='k-means++',n_init=100,max_iter=50,algorithm='full').fit(Data[features])
    labels = kmeans.labels_

    #   Silhouette Coefficient
    ClusterScore= metrics.silhouette_score(Data[features], labels, metric='euclidean')

    #   Saving Scores in a file
    ScoreSave(ClusterScore)

    #   Adding Cluster Label Colomn
    Labeled_CS_Data = Data.assign(Labels=pd.Series(labels).values)

    return Labeled_CS_Data, kmeans


def Clustring(Data, NoOfClusters):
    UniqueUniversities = set(Data["UniversityAppliedUniversityName"])
    # print(len(UniqueUniversities))

    #   To store All cluster's number for individual Universities
    ClusterList = {}

    #   To store cluster's number for individual Universities
    Clusters = []

    #   For preventing to print all cluster info
    j = 0

    #   Getting label wise rows
    for UniversityName in UniqueUniversities:
        #   print(UniversityName)
        for i in range(NoOfClusters):
            #   print("Cluster Number is : ",i)
            #   Getting Unique Universities Cluster wise
            GetUniversities = Data[Data['Labels'] == i]["UniversityAppliedUniversityName"]
            # if j < 30:
            #     print("Cluster Number : " + str(i) + " size is :  ", len(set(GetUniversities)))
            #     print(set(GetUniversities))
            #   j += 1
            if UniversityName in set(GetUniversities):
                Clusters.append(i)

                # print(UniversityName, Clusters, sep='\t\t')
        ClusterList[UniversityName] = list(Clusters)
        Clusters.clear()
    return ClusterList


'''
#################################################
#   To get list of clusters in which University belongs to                              #
###########     start    ###############################
 if UniversityName in set(GetUniversities):
			Clusterlist.append(i)
		i += 1

	print(UniversityName,Clusterlist,sep='\t\t')
	Clusterlist.clear()
#############   end    ###################

#############This is Different###########
for LabelNumber in uniquevalues:
	newdf = CS_Data[CS_Data['LabelNumber'] == LabelNumber]
	print(newdf)
'''


def TakeInput(df):
    #   Taking Input Data
    ToeflIeltsExam = input("Enter IELTS/TOEFL Exam : ")
    ToeflIeltsScore = float(input("Enter IELTS/TOEFL Score : "))
    GREScore = int(input("Enter GRE Score : "))
    CPIScore = float(input("Enter CPI Score : "))
    CollegeScore = float(input("Enter College Overall  Score (Out of 10) : "))
    ATKTAll = int(input("Enter Total ATKTs : "))
    #   UniversityClass = int(input("Enter University Rank India : "))
    #   MajorSubject = input("Enter MajorSubject : ")
    df = pd.DataFrame(data=[[ToeflIeltsExam, ToeflIeltsScore, GREScore, CPIScore, CollegeScore, ATKTAll]],
                      columns=update_features)
    return df

def main():
    #   Imporing Dataset
    ALL_Data = pd.read_csv('D:\BTP\Cluster_Data.csv')

    #   Remove colomns containing NaN values in any colomns
    ALL_Data.dropna(how='any')

    #   Setting Number of Clusters
    NoOfClusters = 20

    #   Binning Of Data -->  Adds New colomns that contains bin value of each samples
    BinnedData = DataBinning(ALL_Data)

    #   print(BinnedData)

    #   For Splliting Binned Data into Testing(80%) And Training(20%)
    msk = np.random.rand(len(BinnedData)) < 0.8

    #   Getting 80% Train Data
    Train_Data = BinnedData[msk]
    #   print('Size of Train Data is  :  ',len(Train_Data))

    #   Getting 20% Validation Data
    Test_Data = BinnedData[~msk]
    #   print('Size of Test Data is  :  ', len(Test_Data))

    #   Saving Test Data for Future Use
    SaveTrainingData(Train_Data)
    SaveTesingData(Test_Data)

    #   Calling main Function --> Adds New colomn that contains cluster value for each sample
    Labeled_Data, KMeansObj = TrainModel(Train_Data, NoOfClusters)

    #   Creates Dictionaries containing University Name as key and Clusters in which a University resides
    GetDictionary = Clustring(Labeled_Data, NoOfClusters)

    #   print(KMeansObj)

    #   Saving Model
    SaveModel(KMeansObj)
    return GetDictionary, Labeled_Data


if __name__ == '__main__':

    #   For new Data Coming up, for prediction
    df = pd.DataFrame()

    # If model file does not exists, train model
    if ModelFile.is_file() == False and DataFile.is_file()==False and ScoreFile.is_file()==False and SaveTrainSet.is_file()==False and SaveTestSet.is_file()==False:
        Dictionary, Get_Labeled_Data = main()
        SaveData(Get_Labeled_Data)
        #   print(Get_Labeled_Data)
        # This is just for checking about Data is right or not, key is University Name and Value is Clusters in which a University Belongs to.
        #for key, value in Dictionary.items():
        #    print(key, value, sep='\t')
    else:
        #   This will give us Labeled Data
        GetData = LoadData()

        #   print('size of Train Data is : ', len(GetData))

        GetTestData = LoadTestingData()
        #   print(len(GetTestData))
        #   print(GetTestData)

        #   Input = TakeInput(df)
        #   Binned_Input = TestDataBinning(Input)
        #   print(Input)
        #   print(Binned_Input)

        # load the model from disk
        LoadedModel = LoadModel(ModelFile)
        Predicted_Value = LoadedModel.predict(GetTestData[features])
        #   print(Predicted_Value)

        #   count = 0
        TrueCount = 0

        #   Getting DataFrame, Indexing starting from 0
        GetTestData = GetTestData.reset_index(drop=True)
        #   print(GetTestData)

        WrongList = []

        #   Iterate through Each Rows
        #   index  is index starts from 0
        #   i is Cluster Number
        for index,i in np.ndenumerate(Predicted_Value):
            #   Taking Unique Universities in each clusters
            GetUniversities = set(GetData[GetData['Labels'] == i]["UniversityAppliedUniversityName"])
            # print(GetUniversities,"\n")

            #   If "Suggested" University is in our cluster
            if GetUniversities.intersection(GetUniversities, GetTestData.loc[[i]]["UniversityAppliedUniversityName"]):
                TrueCount+=1
                #   print(TrueCount)
            else:
                print(GetUniversities)
                print('------------------------------------------------------------------------------------------------')
                print(GetTestData.loc[[i]][PrintFeatures])
                print('------------------------------------------------------------------------------------------------')
                WrongList.append(i)

        print('Length of Test Dataset : ', len(GetTestData))
        print('Wrong Clusters : ', set(WrongList))
        print('Total Number of Wrong Clusters : ', len(set(WrongList)))
        print('Total Wrong Entries in Test Data : ', len(WrongList))

        Accuracy = (len(GetTestData) - len(WrongList)) * 100 / (len(GetTestData))
        print('Cluster Accuracy is : ', Accuracy)

        #   print(index,GetTestData.loc[[index]]["UniversityAppliedUniversityName"])

        #   print(count)
        #   print("\n".join(set(GetUniversities)))
        print('Cluster Performance is : ')
        print(ScoreLoad())
        print('\n\n\n')